# Ensi\InternalMessenger\ChatsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createChat**](ChatsApi.md#createChat) | **POST** /chats | Создание объекта типа Chat
[**deleteChat**](ChatsApi.md#deleteChat) | **DELETE** /chats/{id} | Удаление объекта типа Chat
[**patchChat**](ChatsApi.md#patchChat) | **PATCH** /chats/{id} | Изменение объекта типа Chat
[**searchChats**](ChatsApi.md#searchChats) | **POST** /chats:search | Поиск объектов типа Chat



## createChat

> \Ensi\InternalMessenger\Dto\ChatResponse createChat($create_chat_request)

Создание объекта типа Chat

Создание объекта типа Chat

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\InternalMessenger\Api\ChatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_chat_request = new \Ensi\InternalMessenger\Dto\CreateChatRequest(); // \Ensi\InternalMessenger\Dto\CreateChatRequest | 

try {
    $result = $apiInstance->createChat($create_chat_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChatsApi->createChat: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_chat_request** | [**\Ensi\InternalMessenger\Dto\CreateChatRequest**](../Model/CreateChatRequest.md)|  |

### Return type

[**\Ensi\InternalMessenger\Dto\ChatResponse**](../Model/ChatResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteChat

> \Ensi\InternalMessenger\Dto\EmptyDataResponse deleteChat($id)

Удаление объекта типа Chat

Удаление объекта типа Chat

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\InternalMessenger\Api\ChatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id

try {
    $result = $apiInstance->deleteChat($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChatsApi->deleteChat: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |

### Return type

[**\Ensi\InternalMessenger\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchChat

> \Ensi\InternalMessenger\Dto\ChatResponse patchChat($id, $patch_chat_request)

Изменение объекта типа Chat

Изменение объекта типа Chat

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\InternalMessenger\Api\ChatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | Числовой id
$patch_chat_request = new \Ensi\InternalMessenger\Dto\PatchChatRequest(); // \Ensi\InternalMessenger\Dto\PatchChatRequest | 

try {
    $result = $apiInstance->patchChat($id, $patch_chat_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChatsApi->patchChat: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Числовой id |
 **patch_chat_request** | [**\Ensi\InternalMessenger\Dto\PatchChatRequest**](../Model/PatchChatRequest.md)|  |

### Return type

[**\Ensi\InternalMessenger\Dto\ChatResponse**](../Model/ChatResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchChats

> \Ensi\InternalMessenger\Dto\SearchChatsResponse searchChats($search_chats_request)

Поиск объектов типа Chat

Поиск объектов типа Chat

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\InternalMessenger\Api\ChatsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_chats_request = new \Ensi\InternalMessenger\Dto\SearchChatsRequest(); // \Ensi\InternalMessenger\Dto\SearchChatsRequest | 

try {
    $result = $apiInstance->searchChats($search_chats_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ChatsApi->searchChats: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_chats_request** | [**\Ensi\InternalMessenger\Dto\SearchChatsRequest**](../Model/SearchChatsRequest.md)|  |

### Return type

[**\Ensi\InternalMessenger\Dto\SearchChatsResponse**](../Model/SearchChatsResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

