# Ensi\InternalMessenger\AttachmentsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAttachment**](AttachmentsApi.md#createAttachment) | **POST** /attachments | Загрузка вложения
[**massDeleteAttachments**](AttachmentsApi.md#massDeleteAttachments) | **POST** /attachments:mass-delete | Массовое удаление вложений



## createAttachment

> \Ensi\InternalMessenger\Dto\AttachmentResponse createAttachment($file, $name)

Загрузка вложения

Загрузка вложения

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\InternalMessenger\Api\AttachmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$file = "/path/to/file.txt"; // \SplFileObject | Загружаемый файл
$name = 'name_example'; // string | Имя файла

try {
    $result = $apiInstance->createAttachment($file, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttachmentsApi->createAttachment: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **\SplFileObject****\SplFileObject**| Загружаемый файл | [optional]
 **name** | **string**| Имя файла | [optional]

### Return type

[**\Ensi\InternalMessenger\Dto\AttachmentResponse**](../Model/AttachmentResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## massDeleteAttachments

> \Ensi\InternalMessenger\Dto\EmptyDataResponse massDeleteAttachments($mass_delete_attachments_request)

Массовое удаление вложений

Массовое удаление вложений

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\InternalMessenger\Api\AttachmentsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$mass_delete_attachments_request = new \Ensi\InternalMessenger\Dto\MassDeleteAttachmentsRequest(); // \Ensi\InternalMessenger\Dto\MassDeleteAttachmentsRequest | 

try {
    $result = $apiInstance->massDeleteAttachments($mass_delete_attachments_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AttachmentsApi->massDeleteAttachments: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mass_delete_attachments_request** | [**\Ensi\InternalMessenger\Dto\MassDeleteAttachmentsRequest**](../Model/MassDeleteAttachmentsRequest.md)|  | [optional]

### Return type

[**\Ensi\InternalMessenger\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

