# # MessageReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор сообщения | 
**user_id** | **int** | Идентификатор пользователя | 
**user_type** | **int** | Тип пользователя | 
**chat_id** | **int** | Идентификатор чата | 
**text** | **string** | Содержание сообщения | 
**files** | [**\Ensi\InternalMessenger\Dto\File[]**](File.md) | Прикрепленные файлы | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата создания сообщения | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления сообщения | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


