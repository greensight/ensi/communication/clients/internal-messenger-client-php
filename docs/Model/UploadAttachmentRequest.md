# # UploadAttachmentRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file** | [**\SplFileObject**](\SplFileObject.md) | Загружаемый файл | [optional] 
**name** | **string** | Имя файла | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


