# # AttachmentReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**name** | **string** |  | 
**file** | [**\Ensi\InternalMessenger\Dto\File**](File.md) |  | 
**updated_at** | [**\DateTime**](\DateTime.md) |  | 
**created_at** | [**\DateTime**](\DateTime.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


