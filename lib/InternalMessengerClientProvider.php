<?php

namespace Ensi\InternalMessenger;

class InternalMessengerClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\InternalMessenger\Api\AttachmentsApi',
        '\Ensi\InternalMessenger\Api\ChatsApi',
        '\Ensi\InternalMessenger\Api\MessagesApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\InternalMessenger\Dto\Attachment',
        '\Ensi\InternalMessenger\Dto\AttachmentReadonlyProperties',
        '\Ensi\InternalMessenger\Dto\AttachmentResponse',
        '\Ensi\InternalMessenger\Dto\Chat',
        '\Ensi\InternalMessenger\Dto\ChatDirectionEnum',
        '\Ensi\InternalMessenger\Dto\ChatFillableProperties',
        '\Ensi\InternalMessenger\Dto\ChatIncludes',
        '\Ensi\InternalMessenger\Dto\ChatReadonlyProperties',
        '\Ensi\InternalMessenger\Dto\ChatResponse',
        '\Ensi\InternalMessenger\Dto\CreateChatRequest',
        '\Ensi\InternalMessenger\Dto\EmptyDataResponse',
        '\Ensi\InternalMessenger\Dto\Error',
        '\Ensi\InternalMessenger\Dto\ErrorResponse',
        '\Ensi\InternalMessenger\Dto\ErrorResponse2',
        '\Ensi\InternalMessenger\Dto\File',
        '\Ensi\InternalMessenger\Dto\MassDeleteAttachmentsRequest',
        '\Ensi\InternalMessenger\Dto\Message',
        '\Ensi\InternalMessenger\Dto\MessageReadonlyProperties',
        '\Ensi\InternalMessenger\Dto\ModelInterface',
        '\Ensi\InternalMessenger\Dto\PaginationTypeCursorEnum',
        '\Ensi\InternalMessenger\Dto\PaginationTypeEnum',
        '\Ensi\InternalMessenger\Dto\PaginationTypeOffsetEnum',
        '\Ensi\InternalMessenger\Dto\PatchChatRequest',
        '\Ensi\InternalMessenger\Dto\RequestBodyCursorPagination',
        '\Ensi\InternalMessenger\Dto\RequestBodyMassDelete',
        '\Ensi\InternalMessenger\Dto\RequestBodyOffsetPagination',
        '\Ensi\InternalMessenger\Dto\RequestBodyPagination',
        '\Ensi\InternalMessenger\Dto\ResponseBodyCursorPagination',
        '\Ensi\InternalMessenger\Dto\ResponseBodyOffsetPagination',
        '\Ensi\InternalMessenger\Dto\ResponseBodyPagination',
        '\Ensi\InternalMessenger\Dto\SearchChatsRequest',
        '\Ensi\InternalMessenger\Dto\SearchChatsResponse',
        '\Ensi\InternalMessenger\Dto\SearchChatsResponseMeta',
        '\Ensi\InternalMessenger\Dto\SearchMessagesRequest',
        '\Ensi\InternalMessenger\Dto\SearchMessagesResponse',
        '\Ensi\InternalMessenger\Dto\UploadAttachmentRequest',
        '\Ensi\InternalMessenger\Dto\UserTypeEnum',
    ];

    /** @var string */
    public static $configuration = '\Ensi\InternalMessenger\Configuration';
}
